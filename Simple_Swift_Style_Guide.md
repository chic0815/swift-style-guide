# Swift Style Guide

## Simplified Version

#### Jaesung

© 2019 Jaesung. All Rights Reserved.

> **References** :
> (Swift.org)[https://docs.swift.org]

---

# Table of Contents

1. [Naming](#Naming)
2. [Formatting](#Formatting)
3. [Style](#Style)

---

## Naming

### PascalCase and lowerCamelCase
Use PascalCase for type and protocol names, and lowerCamelCase for everything else.
```Swift
protocol UserDelegate: NSObject { // ... }

class User: UserDelegate {
    var userId: String?
    var nickanme: String?
    
    func didResigter(id: String, nickname: String) { // ... }
    
    func willSignIn() { error in
        guard error != nil else { return }
        print("Error has been occurred: \(error.description)")
    }
    
    func didSignOut() { error in
        // ...
    }
}
```

### Acronyms in names should be all-caps
Except, when it is the start of a name.
```Swift
// GOOD
let httpURL = URL(string: "https://\(url.host)")

var fileURL: URL {
    return destination != nil ? destinationURL : temporaryURL 
}

// AVOID
let httpUrl = ...

var fileUrl: URL {
    return destination != nil ? destinationUrl : temporaryUrl
}

```


### Name booleans like `isConnected`, `hasMember` etc.
**Why?** This makes it clear that they are booleans and not other types.


### General Part First
Names should be written with their most general part first and their most specific
part last.
```Swift
let titleMarginRight: CGFloat
let titleMaginLeft: CGFloat
let actionCancel = UIAlertAction()
```

### Hint About Type
Include a hint about type in a name.
```Swift
// GOOD
let titleLabel: UILabel?
let cancelButton: UIButton?

// AVOID
let cancel: UIButton?
let label: UILabel?
```


### Avoid `*Controller` in  names of classes.
Avoid `*Controller` in names of classes that aren't view controller.
**Why** Controller is an overloaded suffix that doesn't provide information about the 
responsibilities of the class.


---

## Formatting

### One statement per line


### Maximum column of 100 characters
Each line should have a maximum column width of **100** characters.
**Why?** Due to larger screen sizes, we have opted to choose a page guide greater than 80
> **How to set up the option**:
> `Preference` -> `Text Editing` -> `Editing` 
> -> active `Page guide at column` -> set to `100`


### 4 spaces for indented lines
Use 4 spaces to indent lines (= 1 tap in Xcode)


### Whitespace for all line


### Line wrapping
Follow line wrapping (This will be updated)


---

## Style

### Use type Inference
Don't include types where they can be inferred.

```Swift
// GOOD
let isStopped = false

// AVOID
let isStopped: Bool = false
```


### Use `self` explicitly
**Except** for using as parameter.
```Swift
// GOOD
@IBOutlet weak var darkModeSwitch: WKInterfaceSwitch!

func setupUI() {
    guard let value = UserDefaults.standard.value(forKey: "darkmode") as? Bool, 
    value == true else {
        self.darkModeSwitch.setOn(false)    // Use self
        return
    }
    
    self.darkModeSwitch.setOn(true)         // Use self
}
```


### Avoid `Void`
(Function) Omit `Void` return types from function defintions.
```
// GOOD
func setupUI() { // ... }

// AVOID
func setupUI() -> Void { // ... }
```


### Avoid Hungarian Notation
Hungarian notation, such as a leading `g` or `k`, is not used.
```Swift
// GOOD
let secondsPerMinute = 60

// AVOID
let kSecondsPerMinute = 60
let gSecondsPerMinute = 60
let SECONDS_PER_MINUTE = 60
```