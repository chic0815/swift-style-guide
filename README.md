# Swift Style Guide

## Cover

#### Jaesung

© 2019 Jaesung. All Rights Reserved.

> **Introduction:**
> This style guide made by Jaesung, a iOS developer, a winner of WWDC Scholarship 2019
> The guide is mainly based on Apple's excellent Swift standard library style and also
> referred to style guide from Google and Airbnb.
> This style guide will provide 2 types of Swift Style Guide: Simplified Version and 
> General Version.


## Read with efficiency!

### Simplified Style Guide
> **Simplified Style Guide** provides summarized style guide in a page.  
> When you need to see overall contents without details, this document is recommended.

### General Style Guide
> **The Style Guide** provides all of coding examples.  
> When you need to see all of contents, this document is recommended.

### Is Any Issue?
When you find any issue on my style guide, please leave request or message for me.
Your favour makes the guide more perfect.